package ned.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public final class AddAlertItem extends Activity {
	// GUI
	private ListView itemList = null;
	private AutoCompleteTextView itemText = null;
	private RadioButton sortAlpha = null;
	//private RadioButton sortBrand = null;
	
	// Data
	private HashSet<String> items = null;
	private ArrayAdapter<String> itemListAdapter = null;
	private int listStyleID = 0; 

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alert_item);
        
        mapXMLIDs();
        
        // TODO: replace with database values
        setItems(Arrays.asList(FOODS));
        
        initItemList();
        initItemText();
        initSortBy();
    }
    
    private void mapXMLIDs() {
    	itemList = (ListView)findViewById(R.id.alertItems);
    	itemText = (AutoCompleteTextView)findViewById(R.id.alertItem);
    	
    	sortAlpha = (RadioButton)findViewById(R.id.sortAlpha);
    	//sortBrand = (RadioButton)findViewById(R.id.sortBrand);
    	listStyleID = R.layout.narrow_list_items;
    }
    
	@SuppressWarnings("unused")
	private void showDialog(final String text) {
		new AlertDialog.Builder(this).setMessage(text).create().show();
	}
	
	private void setItems(final Collection<String> newItems) {
		// Update items; use Set to remove duplicates
		items = new HashSet<String>(newItems);
		final ArrayList<String> uniqueList = new ArrayList<String>(items);
		
		// Update adapter;
		// only have the list adapter because that is what needs to be sorted
		// and the AutoCompleteTextView will clobber the list adapter if they are set to the same
		itemListAdapter = new ArrayAdapter<String>(this, listStyleID, uniqueList);
		
		// Text adapter is temporary/local variable
		// as we don't need to use it later for sorting, etc.
		// Sort once alphabetically and that's it.
		final ArrayAdapter<String> textAdapter = new ArrayAdapter<String>(this, listStyleID, uniqueList);
		textAdapter.sort(null);
		
		// Update item adapters
		itemList.setAdapter(itemListAdapter);
		itemText.setAdapter(textAdapter);
	}
	
	private void initItemList() {
		// Bind values
		//bindValuesToAdapter(this, itemList, items.toArray(new String[items.size()]));
		
		// Events
		itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// On click, update textbox
				setItemText(((TextView)view).getText().toString());
			}
		});
	}
	
	private void initItemText() {
		// Bind values
		//bindValuesToAdapter(this, itemText, items.toArray(new String[items.size()]));
		itemText.setThreshold(1);
		
		// Events
		itemText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// unused
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// unused
			}

			@Override
			public void afterTextChanged(final Editable s) {
				// Check if string is in item list; if so, select that item
				if (items.contains(s.toString())) {
					itemText.setTextColor(Color.BLUE);
					
					// TODO: enable save/continue button
				}
				else {
					itemText.setTextColor(Color.BLACK);
					
					// TODO: disable save/continue
				}
			}
		});
	}
	
	private void initSortBy() {
        sortAlpha.setOnCheckedChangeListener(new OnCheckedChangeListener() {	
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
				if (isChecked) {
					// Alphabetically
					itemListAdapter.sort(null);
				}
				else {
					// TODO: By Company
					itemListAdapter.sort(new Comparator<String>() {
						@Override
						public int compare(final String lhs, final String rhs) {
							return -lhs.compareTo(rhs);
						}
					});
				}
			}
		});
        
        // Default/firstly sort alphabetically;
        // let user change if needed
        sortAlpha.setChecked(true);
	}
	
	private void setItemText(final String text) {
		itemText.setText(text);
	}
	
	private static final String[] FOODS = new String[] {
		"Pizza", "Burger", "Tofu", "Scrambled Eggs", "Masala", "Paprika", "Apples", "Oranges", "Ziti",
		"burgers", "burgers", "Pizza", "pizza", "tofu", "Tofu",
		"sushi",
		"steak",
		"vegan",
		"indian",
		"chinese",
		"affordable clothing",
		"luxury clothing",
		"cars",
		"trucks",
		"skydiving",
		"hiking",
		"adventure",
		"sports"
	};
}
