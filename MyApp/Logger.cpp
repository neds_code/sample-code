// Own Header
#include "Logger.h"

// C++
#include <cstdio>
#include <ctime>

// Matinnia
#include "Time.h"

// Private to this file
namespace {
    void header();
    void footer();

    // Data
    size_t          messageCount;
    FILE*           logFile;
    Matinnia::Time  timeCreation;
    clock_t         clockCreation;
    char            messageBuffer[512];
}

namespace Matinnia {

void header() {
    char buffer[128] = {0};
    sprintf(buffer, "**********        LOG CREATION     [%.10f]: clock = %10ld        **********", timeCreation.julian(), clockCreation);
    Logger::log(buffer);
}

void footer() {
    const Time      destructTime;
    const clock_t   destructCPU(clock());

    char buffer[256] = {0};

    // Write destruction time
    sprintf(buffer, "**********        LOG DESTRUCTION  [%.10f]: clock = %10ld        **********\n\n"
                    " CPU Time: %f seconds.\n"
                    " Wall Time: %f seconds.\n\n",
            destructTime.julian(), destructCPU, Time::cpuTime(clockCreation, destructCPU), Time::wallTime(timeCreation, destructTime));

    Logger::log(buffer);
}

bool Logger::open(const char* const filename) {
    bool success = false;

    logFile = fopen(filename, "w");

    if (nullptr != logFile) {
        header();
        success = true;
    }

    return success;
}

bool Logger::log(const char* const message) {
    bool success = false;

    // Prevent logging empty strings
    if (nullptr != logFile && nullptr != message && '\0' != message[0]) {
        // fputs() returns non-negative int if successful
        sprintf(messageBuffer, "%u %s    ---    %s\n", ++messageCount, Time().string().c_str(), message);

        if (0 <= fputs(messageBuffer, logFile)) {
            success = true;
        }
    }

    return success;
}

bool Logger::log(const std::string& message) {
    return log(message.c_str());
}

void Logger::close() {
    if (nullptr != logFile) {
        footer();
        fclose(logFile);
        logFile = nullptr;
    }
}

}   // namespace Matinnia