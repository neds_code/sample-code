// Own Header
#include "Time.h"

namespace {
    std::string  timeToString(const time_t time);
    double       timeToJulian(const time_t time);
    double       tmToJulian(const tm& timeStruct);
}

namespace Matinnia {

std::string timeToString(const time_t time) {
    char buffer[128] = {0};

    strftime(buffer, 128, "%H:%M:%S %Y.%m.%d", localtime(&time));

    return std::string(buffer);
}

double tmToJulian(const tm& timeStruct) {
    const int year  = timeStruct.tm_year + 1900;
    const int month = timeStruct.tm_mon + 1;
    const int day   = timeStruct.tm_mday;
    const int hour  = timeStruct.tm_hour;
    const int min   = timeStruct.tm_min;
    const int sec   = timeStruct.tm_sec;

    const int A = (14 - month) / 12;
    const int Y = year + 4800 - A;
    const int M = month + 12*A - 3;

    const long julianDayNumber = static_cast<long>(day + (153*M + 2)/5 + 365*Y + Y/4 - Y/100 + Y/400 - 32045);
		
    return julianDayNumber + (hour - 12.0)/24.0 + min/1440.0 + sec/86400.0;
}

double timeToJulian(const time_t time) {
    // Convert time_t to tm struct (Gregorian date)
    // and from there to julian
    return tmToJulian(*localtime(&time));
}

Time::Time() :
    myTime(time(nullptr))
{}

std::string Time::string() const {
    return timeToString(myTime);
}

double Time::julian() const {
    return timeToJulian(myTime);
}

}   // namespace Matinnia