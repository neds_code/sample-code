// Own Header
#include "Ephemeris.h"

namespace Matinnia {

namespace Ephemeris {

namespace {
    bool isInitialized = false;
}

bool init(const char* const swissPath) {
    swe_set_ephe_path(const_cast<char*>(swissPath));
    isInitialized = true;
    return isInitialized;
}

void close() {
    swe_close();
    isInitialized = false;
}

}   // namespace Ephemeris

}   // namespace Matinnia