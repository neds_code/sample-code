#ifndef MATINNIA_TIME_H
#define MATINNIA_TIME_H

// C++
#include <ctime>
#include <string>

namespace Matinnia {

class Time {
public:
    Time();
    explicit Time(const time_t inTime) : myTime(inTime) {}
    ~Time() {}

    std::string     string() const;
    double          julian() const;
    inline time_t   get() const   { return myTime; }

    inline static double    cpuTime(const clock_t start, const clock_t end) { return (static_cast<double>(end) - static_cast<double>(start)) / CLOCKS_PER_SEC; }
    inline static double    wallTime(const time_t start, const time_t end)  { return difftime(end, start); }
    inline static double    wallTime(const Time& start, const Time& end)    { return wallTime(start.get(), end.get()); }

private:
    time_t  myTime;
};

}   // namespace Matinnia

#endif  // MATINNIA_TIME_H