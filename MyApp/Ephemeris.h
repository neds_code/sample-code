#ifndef MATINNIA_EPHEMERIS_H
#define MATINNIA_EPHEMERIS_H

// Libraries
#include "swephexp.h"

namespace Matinnia {

namespace Ephemeris {

bool    init(const char* const swissPath);
void    close();

}   // namespace Ephemeris

}   // namespace Matinnia

#endif  // MATINNIA_EPHEMERIS_H