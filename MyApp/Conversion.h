#ifndef MATINNIA_CONVERSION_H
#define MATINNIA_CONVERSION_H

// C++
#include <string>

namespace Convert {

std::string toString(const double value) {
    char buffer[64] = {0};
    sprintf(buffer, "%.10f", value);
    return std::string(buffer);
}

}   // namespace Convert

#endif  // MATINNIA_CONVERSION_H