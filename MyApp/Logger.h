#ifndef MATINNIA_LOGGER_H
#define MATINNIA_LOGGER_H

// C++
#include <string>

namespace Matinnia {

// Purpose: Log messages to file.
namespace Logger {
    bool open(const char* const filename);
    bool log(const char* const message);
    bool log(const std::string& message);
    void close();
}   // namespace Logger

}   // namespace Matinnia

#endif  // MATINNIA_LOGGER_H