// Visual Studio Memory Leak Checking
#ifdef _DEBUG
    #define _CRTDBG_MAP_ALLOC
    #include <crtdbg.h>
#endif

// Matinnia
#include "Logger.h"
#include "Ephemeris.h"

using namespace Matinnia;

int main(const int, const char** const) {
#ifdef _DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    //_crtBreakAlloc = 0;
#endif

    Logger::open("myapp_log.txt");

    Logger::log("attempting to call Ephemeris::init()");
    Ephemeris::init("testpath");

    Logger::close();

    return 0;
}