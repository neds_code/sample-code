"use strict";

// No-op console.log() for IE and other suckas :P
if (!window.console) {
	window.console = {
		log: function () {}
	}
}

// FUNCTIONS
function get(ID) {
	return document.getElementById(ID);
}

function getClassItems(name) {
	if (document.getElementsByClassName) {
		return document.getElementsByClassName(name);
	}
	else {
		// IE8
		return document.querySelectorAll(name);
	}
}

function getValue(ID) {
	return get(ID).value;
}

function setValue(ID, newValue) {
	get(ID).value = newValue;
}

function addEventHandler(ID, event, handler) {
	var el = get(ID);
	
	if (el) {
		if (el.addEventListener) {
			el.addEventListener(event, handler);
		}
		else {
			el.attachEvent('on' + event, handler);
		}
	}
}

function enable(ID) {
	get(ID).disabled = false;
}

function disable(ID) {
	get(ID).disabled = true;
}
		
function getHTML(ID) {
	return get(ID).innerHTML;
}
		
function getText(ID) {
	return get(ID).textContent;
}
		
function changeColor(name, color) {
	get(name).style.color = color;
}
		
function changeTitle(newTitle) {
	document.title = newTitle;
}
		
function getElementWidth(elementID) {
	return get(elementID).clientWidth;
}
		
function getWindowWidth() {
	return (0 < window.innerWidth) ? window.innerWidth : screen.width;
}
		
function prependHTML(elementID, html) {
	get(elementID).innerHTML = html + get(elementID).innerHTML;
}
		
function prependText(elementID, text) {
	get(elementID).innerText = text + get(elementID).innerText;
}
		
function appendHTML(elementID, html) {
	get(elementID).innerHTML += html;
}

function appendHTMLLine(elementID, html) {
	get(elementID).innerHTML += html + '\n';
}
		
function appendText(elementID, text) {
	get(elementID).innerText += text;
}

function appendTextLine(elementID, text) {
	get(elementID).innerText += text + '<br/>';
}

function setHTML(elementID, html) {
	get(elementID).innerHTML = html;
}

function setText(elementID, text) {
	get(elementID).innerText = text;
}

function resizeImage(imageID, newWidth, newHeight) {
	var image = get(imageID);
	image.width = newWidth;
	image.height = newHeight;
}

function fitElementInContainer(containerID, elementID) {
	// Get container dimensions
	var element = get(elementID);
	
	// New width is 80% of the container
	var newWidth = 0.8 * getElementWidth(containerID);
	
	// New height must keep original aspect ratio
	var aspectRatio = element.height / element.width;
	var newHeight = newWidth * aspectRatio;
	
	resizeImage(elementID, newWidth, newHeight);
}

function getStringBoundedLeft(inputString, leftDelimiter) {
	var result = "";
	
	if (0 < inputString.length && 0 < leftDelimiter.length) {
		result = inputString.substring(inputString.indexOf(leftDelimiter) + leftDelimiter.length, inputString.length);
	}

	return result;
}
			
function getCookie(keyString) {
	var result = '';
	var fullCookie = decodeURIComponent(document.cookie);
				
	// Get the first index
	var firstIndex = fullCookie.indexOf(keyString);
				
	if (-1 < firstIndex) {
		// Find the last index
		var lastIndex = fullCookie.indexOf(";", firstIndex + keyString.length);
					
		// If only one cookie, set lastIndex to end of cookie string
		if (-1 == lastIndex) {
			lastIndex = fullCookie.length;
		}
					
		// Get the full cookie string
		var cookie = fullCookie.substring(firstIndex, lastIndex);
					
		result = getStringBoundedLeft(cookie, "=");
	}
				
	return result;
}
			
function setCookie(key, value) {
	document.cookie = encodeURIComponent(key) + '=' + encodeURIComponent(value);
}

function addLoadEvent(func) {
	var existingLoad = window.onload;
	
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
		window.onload = function() {
			if (existingLoad) {
				existingLoad();
			}
		
			func();
		}
	}
}

function updateSubmitButton(className, submitID) {
	var inputs = getClassItems(className);
	var ready = true;
		
	for (var i = 0; i < inputs.length; ++i) {
		if (0 === inputs[i].value.length) {
			ready = false;
			break;
		}
	}
		
	if (ready) {
		enable(submitID);
	}
	else {
		disable(submitID);
	}
}

function createAJAX() {
	var httpRequest;

	if (window.XMLHttpRequest) {
		httpRequest = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		try {
			httpRequest = new ActiveXObject('Msxml2.XMLHTTP');
			console.log('using Msxml2.XMLHTTP');
		}
		catch (e) {
			try {
				httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
				console.log('using Microsoft.XMLHTTP');
			}
			catch (e) {
				console.log('Browser does not support AJAX.');
				return false;
			}
		}
	}

	return httpRequest;
}

function postAsync(URL, callback, data) {
	var request = createAJAX();
	
	if (request) {
		request.onreadystatechange = function() {
			if (4 === request.readyState) {
				console.log('AJAX POST response received. Calling callback function.');
				callback(request.responseText);
			}
		}
		
		request.open('POST', URL, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send(encodeURIComponent(data));
		
		console.log('Sent async AJAX POST to: \'' + URL + '\'');
		console.log('POST data: ' + data);
	}
	else {
		console.log('Unable to create async AJAX POST for: ' + URL);
	}
}

function requestAsync(URL, callback) {
	// Don't return anything. Callback happens after response received.
	var request = createAJAX();
	
	if (request) {
		request.onreadystatechange = function() {
			if (4 === request.readyState) {
				console.log('AJAX response received. Calling callback function.');
				callback(request.responseText);
			}
		}
		
		var requestString = uniquifyGET(URL);
		request.open('GET', requestString, true);
		request.send();
		console.log('Sent async AJAX request: ' + requestString);
	}
	else {
		console.log('Unable to create async AJAX request for: ' + URL);
	}
}

function requestSync(URL) {
	// Return response; can wait since synchronous.
	var request = createAJAX();
	var response = '';

	if (request) {
		// unique-ify the requested URL to prevent request caching
		var requestString = uniquifyGET(URL);
		request.open('GET', requestString, false);
		request.send();
		console.log('Sent sync AJAX request: ' + requestString);

		response = request.responseText;
		console.log('AJAX response: ' + response);
	}
	else {
		console.log('Unable to create sync AJAX request for: ' + URL);
	}
	
	return response;
}

function stringContains(checkString, substring) {
	if (-1 === checkString.search(new RegExp(substring))) {
		return false;
	}
	else {
		return true;
	}
}

function isString(potentialString) {
	return ('string' === typeof potentialString || potentialString instanceof String);
}

function uniquifyGET(URL) {
	var result = URL;
	
	if (isString(URL)) {
		// Two options:
		// 1. path.extension                  --> path.extension?time=182848939
		// 2. path.extension?existingVar=foo  --> path.extension?existingVar=foo&time=18239028
		var time = 'time=' + new Date().getTime();
		
		// Check path for existing '?'
		if (stringContains(URL, '\\?')) {
			result += '&';
		}
		else {
			result += '?';
		}
		
		result += time;
	}
	
	return result;
}