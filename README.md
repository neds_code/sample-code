# README #

This, so far at least, is sample code to share what my code looks like. I'm always interested in writing better code, better names, more elegance, etc., always with a strong dose of practicality.

The files are a little haphazard as of now (Aug 26, 2014), but more code may be added. This is kind of a diverse sampling. Languages include:

* Android (Java)
* C++
* Javascript
* CSS3